# Software AGV

Bienvenue sur le Repo logiciel de l'agv sous ROS.

Cette section contient tout le code nécessaire pour lancer l'AGV que ce soit en simulation ou sur le robot physique.

Le midleware ROS nécessite une prise en main qui requière plusieurs heures de travail, mais cela varie selon les profils. Le wiki contient plus de détails, notamment où commencer l'apprentissage de ROS.

Pour voir les détails, veuillez visiter le wiki qui se trouve dans le lien suivant,

https://gitlab.com/MIQ-AGV2/Prog/-/wikis/home

Pour toute question,  n'hésiter pas à me contacter sur cette adresse mail : mouad.abrini@insa-strasbourg.fr

Cordialement,

Mouad ABRINI
