#!/usr/bin/env python

from __future__ import print_function

from geometry_msgs.msg import Twist

from epos_ros_driver.srv import BringupMotor, BringupMotorResponse
from std_msgs.msg import String
from sensor_msgs.msg import JointState
import rospy
import time
from ctypes import *

import numpy as np


path = '/opt/EposCmdLib_6.7.1.0/lib/v8/libEposCmd.so.6.7.1.0'

cdll.LoadLibrary(path)


# Defining return variables from Library Functions
ret = 0
pErrorCode = c_uint()
pDeviceErrorCode = c_uint()

# Defining a variable NodeID and configuring connection
nodeID = 1
baudrate = 1000000
timeout = 500

# Configure desired motion profile
acceleration = 40  # rpm/s, up to 1e7 would be possible
deceleration = 140  # rpm/s
a = 1
epos = ""
epos1 = ""

keyHandle = 0
keyHandle1 = 0
on = 0

def compute_diff(position, prev_position):
    diff = position - prev_position
    if abs(diff) > (2**32)//2:
        if position < prev_position:
            diff = diff%(2**32)
        else:
            temp = (prev_position - position)%(2**32)
            diff = -temp

    print(position, prev_position, "diff = ", diff)
    return (diff/(2**12))*2*np.pi


def listener():
    rospy.Subscriber("cmd_vel", Twist, callback)
    #rospy.Subscriber("chatter1", String, GetPositionIs)

    # spin() simply keeps python from exiting until this node is stopped
prev_position, position, position_init, prev_position1, position1, position_init1 = 0, 0, 0, 0, 0, 0
b = 1
def talker():
    pub = rospy.Publisher('joint_states', JointState, queue_size=10)
    rate = rospy.Rate(10)# 10hz
    hello_str = JointState()
    s = 0
    s1 = 0
    while not rospy.is_shutdown():
        if on:
            global epos, position, prev_position, b, position_init, epos1, keyHandle, keyHandle1, prev_position1, position1, position_init1
            dt = 1 / 5
            pPositionIs = c_long()
            pErrorCode = c_uint()
            pPositionIs1 = c_long()
            pErrorCode1 = c_uint()
            ret = epos.VCS_GetPositionIs(keyHandle, nodeID, byref(pPositionIs), byref(pErrorCode))
            ret1 = epos1.VCS_GetPositionIs(keyHandle1, nodeID, byref(pPositionIs1), byref(pErrorCode1))
            if b:
                prev_position = pPositionIs.value
                prev_position1 = pPositionIs1.value
                b = 0
            position = (pPositionIs.value - 0)
            position1 = (pPositionIs1.value - 0)
            #print("1:", pPositionIs.value)
            #print("2:", pPositionIs1.value)

            diff = compute_diff(position, prev_position)
            diff1 = compute_diff(position1, prev_position1)

            s += diff
            s1 += diff1

            #print(s)
            #print(s1)



            prev_position = position
            prev_position1 = position1


            hello_str.header.stamp = rospy.Time.now()
            hello_str.name = ['front_left_wheel_joint', 'front_right_wheel_joint']
            hello_str.position = [s1, -s]
            pub.publish(hello_str)
            rate.sleep()

def open_communication(req):
    global a, epos, keyHandle, on, epos1, keyHandle1
    a = 1
    print("Returning %s"%(req.motor_name))
    epos = CDLL(path)
    epos1 = CDLL(path)
    keyHandle = epos.VCS_OpenDevice(b'EPOS4', b'MAXON SERIAL V2', b'USB', b'USB0',
                                              byref(pErrorCode))  # specify EPOS version and interface
    epos.VCS_SetProtocolStackSettings(keyHandle, baudrate, timeout, byref(pErrorCode))  # set baudrate
    epos.VCS_ClearFault(keyHandle, nodeID, byref(pErrorCode))  # clear all faults
    #epos.VCS_ActivateProfilePositionMode(keyHandle, nodeID, byref(pErrorCode)) # activate profile position mode
    epos.VCS_ActivateProfileVelocityMode(keyHandle, nodeID, byref(pErrorCode))
    epos.VCS_SetEnableState(keyHandle, nodeID, byref(pErrorCode))  # enable device

    keyHandle1 = epos1.VCS_OpenDevice(b'EPOS4', b'MAXON SERIAL V2', b'USB', b'USB1',
                                    byref(pErrorCode))  # specify EPOS version and interface
    epos1.VCS_SetProtocolStackSettings(keyHandle1, baudrate, timeout, byref(pErrorCode))  # set baudrate
    epos1.VCS_ClearFault(keyHandle1, nodeID, byref(pErrorCode))  # clear all faults
    #epos.VCS_ActivateProfilePositionMode(keyHandle, nodeID, byref(pErrorCode)) # activate profile position mode
    epos1.VCS_ActivateProfileVelocityMode(keyHandle1, nodeID, byref(pErrorCode))
    epos1.VCS_SetEnableState(keyHandle1, nodeID, byref(pErrorCode))  # enable device
    epos.VCS_SetVelocityProfile(keyHandle, nodeID, acceleration, deceleration,
                                byref(pErrorCode))
    epos1.VCS_SetVelocityProfile(keyHandle1, nodeID, acceleration, deceleration,
                                byref(pErrorCode))

    on = 1
    while a:
        pass

    epos.VCS_SetDisableState(keyHandle, nodeID, byref(pErrorCode))  # disable device
    epos.VCS_CloseDevice(keyHandle, byref(pErrorCode))
    epos1.VCS_SetDisableState(keyHandle1, nodeID, byref(pErrorCode))  # disable device
    epos1.VCS_CloseDevice(keyHandle1, byref(pErrorCode))
    return BringupMotorResponse(req.motor_name)

def close_communication(req):
    global a
    a = 0
    return BringupMotorResponse(req.motor_name)

def set_speed(vit1, vit2):
    global epos, keyHandle, keyHandle1, epos1


    epos.VCS_MoveWithVelocity(keyHandle, nodeID, -int(vit1), byref(pErrorCode))
    epos1.VCS_MoveWithVelocity(keyHandle1, nodeID, int(vit2), byref(pErrorCode))


def add_two_ints_server():
    #rospy.init_node('motor_com')
    s = rospy.Service('open_communication', BringupMotor, open_communication)
    s = rospy.Service('close_communication', BringupMotor, close_communication)
    print("Ready !")

def callback(msg):
    #rospy.loginfo("Received a /cmd_vel message!")
    #rospy.loginfo("Linear Components: [%f, %f, %f]" % (msg.linear.x, msg.linear.y, msg.linear.z))
    #rospy.loginfo("Angular Components: [%f, %f, %f]" % (msg.angular.x, msg.angular.y, msg.angular.z))
    L = 0.61
    R = 0.135/2
    vit1 = (2 * msg.linear.x + msg.angular.z * L) / (R) * (30 / 3.14)
    vit2 = (2 * msg.linear.x - msg.angular.z * L) / (R) * (30 / 3.14)
    set_speed(vit1, vit2)


def GetPositionIs(data):
    global epos, position, prev_position, b, position_init
    dt = 1/5
    pPositionIs = c_long()
    pErrorCode = c_uint()
    ret = epos.VCS_GetPositionIs(keyHandle, nodeID, byref(pPositionIs), byref(pErrorCode))
    if b:
        position_init = pPositionIs.value
        b = 0
    position = (pPositionIs.value - position_init)
    #print("speed=", (3.14/180)*(position-prev_position)/dt)
    print((position/12)%360)
    print("diff= ", (position-prev_position)/12)
    prev_position = position
    #return pPositionIs.value  # motor steps

if __name__ == "__main__":
    rospy.init_node('motor', anonymous=True)
    add_two_ints_server()
    listener()
    talker()
    rospy.spin()