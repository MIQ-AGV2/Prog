#!/usr/bin/env python


path = '/opt/EposCmdLib_6.7.1.0/lib/x86_64/libEposCmd.so.6.7.1.0'

import serial
import time
from ctypes import *

# Load library
cdll.LoadLibrary(path)


# Defining return variables from Library Functions
ret = 0
pErrorCode = c_uint()
pDeviceErrorCode = c_uint()

# Defining a variable NodeID and configuring connection
nodeID = 1
baudrate = 1000000
timeout = 500

# Configure desired motion profile
acceleration = 2000  # rpm/s, up to 1e7 would be possible
deceleration = 3000  # rpm/s


# Query motor position
def GetPositionIs():
    pPositionIs = c_long()
    pErrorCode = c_uint()
    ret = epos.VCS_GetPositionIs(keyHandle, nodeID, byref(pPositionIs), byref(pErrorCode))
    return pPositionIs.value  # motor steps


def GetSpeedIs():
    pVelocityIs = c_long()
    pErrorCode = c_uint()
    ret = epos.VCS_GetVelocityIs(keyHandle, nodeID, byref(pVelocityIs), byref(pErrorCode))
    return pVelocityIs.value  # motor steps


# Move to position at speed
def MoveToPositionSpeed(target_position, target_speed):
    while True:
        if target_speed != 0:
            epos.VCS_SetPositionProfile(keyHandle, nodeID, target_speed, acceleration, deceleration,
                                        byref(pErrorCode))  # set profile parameters
            epos.VCS_MoveToPosition(keyHandle, nodeID, target_position, True, True,
                                    byref(pErrorCode))  # move to position
        elif target_speed == 0:
            epos.VCS_HaltPositionMovement(keyHandle, nodeID, byref(pErrorCode))  # halt motor
        true_position = GetPositionIs()
        if true_position == target_position:
            break


def setSpeed(target_speed):
    while True:
        if target_speed != 0:
            epos.VCS_SetVelocityProfile(keyHandle, nodeID, acceleration, deceleration,
                                        byref(pErrorCode))  # set profile parameters
            epos.VCS_MoveWithVelocity(keyHandle, nodeID, target_speed, byref(pErrorCode))  # move to position
        elif target_speed == 0:
            epos.VCS_HaltPositionMovement(keyHandle, nodeID, byref(pErrorCode))  # halt motor
        true_position = GetPositionIs()
        print(true_position)

"""
if __name__ == "__main__":
    # Initiating connection and setting motion profile
    keyHandle = epos.VCS_OpenDevice(b'EPOS4', b'MAXON SERIAL V2', b'USB', b'USB0',
                                    byref(pErrorCode))  # specify EPOS version and interface
    epos.VCS_SetProtocolStackSettings(keyHandle, baudrate, timeout, byref(pErrorCode))  # set baudrate
    epos.VCS_ClearFault(keyHandle, nodeID, byref(pErrorCode))  # clear all faults
    # epos.VCS_ActivateProfilePositionMode(keyHandle, nodeID, byref(pErrorCode)) # activate profile position mode
    epos.VCS_ActivateProfileVelocityMode(keyHandle, nodeID, byref(pErrorCode))
    epos.VCS_SetEnableState(keyHandle, nodeID, byref(pErrorCode))  # enable device

    MoveToPositionSpeed(20000,500) # move to position 20,000 steps at 5000 rpm/s
    print('Motor position: %s' % (GetPositionIs()))
    time.sleep(1)

    MoveToPositionSpeed(0,5000) # move to position 0 steps at 2000 rpm/s
    print('Motor position: %s' % (GetPositionIs()))
    time.sleep(1)

    epos.VCS_SetDisableState(keyHandle, nodeID, byref(pErrorCode))  # disable device
    epos.VCS_CloseDevice(keyHandle, byref(pErrorCode))  # close device
"""
a = True


class Motor:
    def __init__(self):
        self.epos = CDLL(path)

    def initialize(self):
        self.keyHandle = self.epos.VCS_OpenDevice(b'EPOS4', b'MAXON SERIAL V2', b'USB', b'USB0',
                                        byref(pErrorCode))  # specify EPOS version and interface
        self.epos.VCS_SetProtocolStackSettings(self.keyHandle, baudrate, timeout, byref(pErrorCode))  # set baudrate
        self.epos.VCS_ClearFault(self.keyHandle, nodeID, byref(pErrorCode))  # clear all faults
        # epos.VCS_ActivateProfilePositionMode(keyHandle, nodeID, byref(pErrorCode)) # activate profile position mode
        self.epos.VCS_ActivateProfileVelocityMode(self.keyHandle, nodeID, byref(pErrorCode))
        self.epos.VCS_SetEnableState(self.keyHandle, nodeID, byref(pErrorCode))  # enable device

        """
        MoveToPositionSpeed(20000,500) # move to position 20,000 steps at 5000 rpm/s
        print('Motor position: %s' % (GetPositionIs()))
        time.sleep(1)

        MoveToPositionSpeed(0,5000) # move to position 0 steps at 2000 rpm/s
        print('Motor position: %s' % (GetPositionIs()))
        time.sleep(1)
        """
        while a:
            pass

        self.epos.VCS_SetDisableState(self.keyHandle, nodeID, byref(pErrorCode))  # disable device
        self.epos.VCS_CloseDevice(self.keyHandle, byref(pErrorCode))

    def getVelocity(self):
        pVelocityIs = c_long()
        pErrorCode = c_uint()
        ret = self.epos.VCS_GetVelocityIs(self.keyHandle, nodeID, byref(pVelocityIs), byref(pErrorCode))
        if pVelocityIs.value > 10000:
            return pVelocityIs.value - 2**32
        return pVelocityIs.value

    def closeWithThread(self):
        global a
        a = False

    def initWithThread(self):
        global m
        m.initialize()

    def speed(self, target, delay):
        self.epos.VCS_SetVelocityProfile(self.keyHandle, nodeID, acceleration, deceleration,
                                    byref(pErrorCode))  # set profile parameters
        self.epos.VCS_MoveWithVelocity(self.keyHandle, nodeID, target, byref(pErrorCode))

        time.sleep(delay)

        #self.epos.VCS_MoveWithVelocity(self.keyHandle, nodeID, 0, byref(pErrorCode))




    def initt(self):
        Thread(target=self.initWithThread).start()

    def close(self):
        Thread(target=self.closeWithThread).start()


from threading import Thread

m = Motor()


def func3():
    global m, a
    while a:
        print(m.getVelocity())

if __name__ == '__main__':
    m.initt()
    time.sleep(2)
    m.speed(10000, 0)
    print(m.getVelocity())
    time.sleep(10)
    m.close()





