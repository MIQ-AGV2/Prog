#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from epos_ros_driver.srv import BringupMotor, BringupMotorResponse

def add_two_ints_client(name):
    rospy.wait_for_service('add_two_ints')
    try:
        add_two_ints = rospy.ServiceProxy('add_two_ints', BringupMotor)
        resp1 = add_two_ints(name)
        return resp1.result
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def usage():
    return "%s [x y]"%sys.argv[0]

if __name__ == "__main__":
    print("hum {}".format(add_two_ints_client("maxon")))
